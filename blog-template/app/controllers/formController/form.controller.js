'use strict';

serviceJoinApp.controller('formCtrl', ['$scope','imageUplaod', 'Upload', 'formService', function ($scope, imageUplaod, Upload, formService) {
    $scope.stacksavingsForm = {};
        $scope.saveForm = function(file, form_name){
            imageUplaod.getSignedUrl(file.type, file.name).then(function(result){
                var src = result.data.resultUrl + '?v=' + Math.random();
                $scope.form.bgImage = src;
                $scope.form.formname = form_name;
                $scope.stacksavingsForm.data = angular.copy($scope.form);
                formService.saveForm($scope.stacksavingsForm).then(function(result){
                    debugger;
                })
                Upload.http({
                    method : "PUT",
                    url    : result.data.oneTimeUploadUrl,
                    headers: {
                            'Content-Type': file.type != '' ? file.type : 'application/octet-stream'
                        },
                    data   : file
                    }).then(function (resp) {
                        $('.post-content-text').css('background-image', 'url(\'' + src + '\')');
                        $('#thumbnail-img').hide();
                    },function (response) {
                        if (response.status > 0)
                        self.errorMsg = response.status + ': ' + response.data;
                    }, function (evt) {
                        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        $scope.picFile.progress = file.progress;
                });
            });
        }
    }]);


