'use strict';

serviceJoinApp.service('formService', ['$q', '$resource', 'apiBaseUrl', '$filter', '$rootScope', '$http', function ($q, $resource, apiBaseUrl, $filter, $rootScope, $http) {

        // AngularJS will instantiate a singleton by calling "new" on this function
        var api = $resource(apiBaseUrl + 'stacksavings/form');
        return {
            saveForm: function (formData) {
                var deferred = $q.defer();
                var returnData =  $http.post(apiBaseUrl + 'stacksavings/form', formData, {headers: {'Content-Type': 'text/plain'}});
                returnData.then(function(response){
                    deferred.resolve(response);
                })
               /* api.save(formData, function (response) {
                    deferred.resolve(response);
                });*/
                return deferred.promise;
            },

        }
    }]);
