serviceJoinApp.directive("editableImage", function (imageUplaod, Upload, apiBaseUrl) {
        return {
           templateUrl: '../app/directives/file-upload/file-upload-tpl.html',
            restrict: 'E',
            link: function (scope, element, attrs) {
                scope.picFile ={};
                scope.uploadFile = function(file, div){
                    var $progress = $('#' + div);
                   imageUplaod.getSignedUrl(file.type, file.name).then(function(result){
                        Upload.http({
                            method : "PUT",
                            url    : result.data.oneTimeUploadUrl,
                            headers: {
                                'Content-Type': file.type != '' ? file.type : 'application/octet-stream'
                            },
                            data   : file
                        }).then(function (resp) {
                            var src = result.data.resultUrl + '?v=' + Math.random();
                            $('.post-content-text').css('background-image', 'url(\'' + src + '\')');
                            $('#thumbnail-img').hide();
                        },function (response) {
                            if (response.status > 0)
                                self.errorMsg = response.status + ': ' + response.data;
                        }, function (evt) {
                            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                            scope.picFile.progress = file.progress;
                            $progress.html(file.progress);
                        });
                   })
               }
            }
        }
    });